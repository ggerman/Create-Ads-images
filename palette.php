<?php

$palette['hex'][0] = '#6D8DC5';
$palette['hex'][1] = '#607745';
$palette['hex'][2] = '#C17039';
$palette['hex'][3] = '#C7B671';
$palette['hex'][4] = '#E1DDCE';

$palette['rgb'][0] = '109,141,197';
$palette['rgb'][1] = '96,119,69';
$palette['rgb'][2] = '193,112,57';
$palette['rgb'][3] = '199,182,113';
$palette['rgb'][4] = '225,221,206';

$palette['font']['hex'] = '#000000';
$palette['font']['rgb'] = '0, 0, 0';

$colorLover['palette']['Eight_Pointed_Stars'] = $palette;

$palette['hex'][0] = '#4E0B5A';
$palette['hex'][1] = '#5F166C';
$palette['hex'][2] = '#762883';
$palette['hex'][3] = '#953EA4';
$palette['hex'][4] = '#C153D4';

$palette['rgb'][0] = '78,11,90';
$palette['rgb'][1] = '95,22,108';
$palette['rgb'][2] = '118,40,131';
$palette['rgb'][3] = '149,62,164';
$palette['rgb'][4] = '193,83,212';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Plums'] = $palette;

$palette['hex'][0] = '#AE43BA';
$palette['hex'][1] = '#ED3D6A';
$palette['hex'][2] = '#F3E796';
$palette['hex'][3] = '#90EDCE';
$palette['hex'][4] = '#ACF9D0';

$palette['rgb'][0] = '174,67,186';
$palette['rgb'][1] = '237,61,106';
$palette['rgb'][2] = '243,231,150';
$palette['rgb'][3] = '144,237,206';
$palette['rgb'][4] = '172,249,208';

$palette['font']['hex'] = '#000000';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Evolving'] = $palette;


$palette['hex'][0] = '#AC1F23';
$palette['hex'][1] = '#2C1F92';
$palette['hex'][2] = '#2B394D';
$palette['hex'][3] = '#BEBEBC';
$palette['hex'][4] = '#DCE7EB';

$palette['rgb'][0] = '172,31,35';
$palette['rgb'][1] = '44,31,146';
$palette['rgb'][2] = '43,57,77';
$palette['rgb'][3] = '190,190,188';
$palette['rgb'][4] = '220,231,235';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Little_Boy'] = $palette;

$palette['hex'][0] = '#842DC6';
$palette['hex'][1] = '#BF2ADD';
$palette['hex'][2] = '#7225F7';

$palette['rgb'][0] = '132,45,198';
$palette['rgb'][1] = '191,42,221';
$palette['rgb'][2] = '114,37,247';

$palette['font']['hex'] = '#2D3234';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Cizme_Copii_in_anti'] = $palette;

$palette['hex'][0] = '#C9EFCE';
$palette['hex'][1] = '#D4CEE3';
$palette['hex'][2] = '#66DFB7';
$palette['hex'][3] = '#1BA779';
$palette['hex'][4] = '#70E482';

$palette['rgb'][0] = '201,239,206';
$palette['rgb'][1] = '212,206,227';
$palette['rgb'][2] = '102,223,183';
$palette['rgb'][3] = '27,167,121';
$palette['rgb'][4] = '112,228,130';

$palette['font']['hex'] = '#000000';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Hurricat_CLAD_1LP'] = $palette;

$palette['hex'][0] = '3C4259';
$palette['hex'][1] = 'EFB608';
$palette['hex'][2] = 'F4A30D';
$palette['hex'][3] = 'F29115';
$palette['hex'][4] = 'BE0407';

$palette['rgb'][0] = '60,66,89';
$palette['rgb'][1] = '239,182,8';
$palette['rgb'][2] = '244,163,13';
$palette['rgb'][3] = '242,145,21';
$palette['rgb'][4] = '190,4,7';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Forbidden_Ember'] = $palette;

$palette['hex'][0] = '#69D2E7';
$palette['hex'][1] = '#A7DBD8';
$palette['hex'][2] = '#E0E4CC';
$palette['hex'][3] = '#F38630';
$palette['hex'][4] = '#FA6900';

$palette['rgb'][0] = '105,210,231';
$palette['rgb'][1] = '167,219,216';
$palette['rgb'][2] = '224,228,204';
$palette['rgb'][3] = '243,134,48';
$palette['rgb'][4] = '250,105,0';

$palette['font']['hex'] = '#312F2F';
$palette['font']['rgb'] = '49, 47, 47';

$colorLover['palette']['Giant_Goldfish'] = $palette;

$palette['hex'][0] = '#ECD078';
$palette['hex'][1] = '#D95B43';
$palette['hex'][2] = '#C02942';
$palette['hex'][3] = '#542437';
$palette['hex'][4] = '#53777A';

$palette['rgb'][0] = '236,208,120';
$palette['rgb'][1] = '217,91,67';
$palette['rgb'][2] = '192,41,66';
$palette['rgb'][3] = '84,36,55';
$palette['rgb'][4] = '83,119,122';

$palette['font']['hex'] = '#000000';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['Thought_Provoking'] = $palette;

$palette['hex'][0] = '#351330';
$palette['hex'][1] = '#424254';
$palette['hex'][2] = '#64908A';
$palette['hex'][3] = '#E8CAA4';
$palette['hex'][4] = '#CC2A41';

$palette['rgb'][0] = '53,19,48';
$palette['rgb'][1] = '66,66,84';
$palette['rgb'][2] = '100,144,138';
$palette['rgb'][3] = '232,202,164';
$palette['rgb'][4] = '204,42,65';

$palette['font']['hex'] = '#ffffff';
$palette['font']['rgb'] = '255, 255, 255';

$colorLover['palette']['you_are_beautiful'] = $palette;
?>