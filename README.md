Create Ads images
===========================
`Create Ads images`

Get Flickr images
=================
`./getimage.rb {URL}`

Php Palette file
================
Vector: palette.php

Authors
===================

*Created by* Germán Alberto Giménez Silva [@gsgerman](https://twitter.com/gsgerman)

*E-mail:* ggerman@gmail.com

*Date:* 20-08-14