<?php

$dir = realpath('.');
$path = $dir."/tmp/";
$images = dir($path);

while ($file = $images->read())
{
  $fileDescription = getimagesize($path.$file);
  if ($fileDescription['mime'] == 'image/jpeg')
  {
    $vector[] = $file;
  }
}

$aLength = count($vector);
$index = rand(0, $aLength-1);

$fileName = $vector[$index];

// Content type
header('Content-Type: image/png');

$maxwidth = 600;
$maxheight = 450;

$source = imagecreatefromjpeg($path.$fileName);

$width = imagesx($source);
$height = imagesy($source);

if ($height > $width) 
{   
  $ratio = $maxheight / $height;  
  $newheight = $maxheight;
  $newwidth = $width * $ratio; 
} else {
  $ratio = $maxwidth / $width;   
  $newwidth = $maxwidth;  
  $newheight = $height * $ratio;   
}

$newimg = imagecreatetruecolor($newwidth,$newheight); 

imagecopyresized($newimg, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

imagepng($newimg, null, 09);