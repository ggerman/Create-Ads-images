#!/usr/bin/env ruby

require 'open-uri'
require 'nokogiri' 
require 'uri'
require 'fileutils'

def getHtml(url)
  flickr = open(url)
  return flickr.read
end

def hashFromFlickr(text)
  register = Hash.new
  field = Hash.new  
  hash = text.lstrip.split(': ')[1].to_str.slice(2 , text.to_str.length - 20)
  hash = hash.to_s.gsub("\"", "").split("},{")
  
  count = 0
  hash.each do |line|
    count = count + 1
    
    line.split("\r\n").each do |a|
      a.split(",").each do |r|
	field[r.split(":")[0].to_s] = r.split(":")[1].to_s
       end
       register = field
    end
  end
  return register
end


def obtainImage(url)
  page = Nokogiri::HTML(getHtml(url))
 
  images = page.css("script")
  
  
  i = 6
  if  images[i].to_s =~ /ratioWidthBasis/i
      i = 6
  else
      i = 7
  end

  javascript = images[i].to_str.split('function')[10].slice(
		      images[i].to_str.split('function')[10].index('var photo'), 
		      images[i].to_str.split('function')[10].index('"}]')
		)
  
  imagesDetails = javascript.to_str.slice(javascript.to_str.index("{") + 2, javascript.to_str.length)
  imagesDetails.split(",\n").each do |e|
    @key = e.lstrip.split(' ')[0].slice(0, e.lstrip.split(' ')[0].to_str.length- 1)
    
    case @key
      when 'id'
	@id = e.lstrip.split(': ')[1]
	@id
      when 'baseURL'
	@baseURL = e.lstrip.split(': ')[1].gsub("https", "http")
	@baseURL
      when 'sizeMap'
	@hash = hashFromFlickr(e) 
    end
  end
  url = @baseURL.split("/");
  
  if @hash["fileExtension"].to_s == '_b'
    img = @baseURL
    img = img.to_s.slice(1, img.to_str.length-6)
    img = img.to_s + '_b' + '.jpg'
  else
    img = url[0].to_s+"//"+url[2].to_s+"/"+url[3].to_s+"/"+url[4].to_s+"/"+@id.to_s+"_"+@hash["secret"].to_s+@hash["fileExtension"].to_s+".jpg"
    img = img.to_s.slice(1, img.to_str.length)
  end
  

  puts img
  
  remote_data = open(img).read
  my_local_file = open('tmp/'+@id.to_s+"_"+@hash["secret"].to_s+@hash["fileExtension"].to_s+".jpg", "w") 
  
  my_local_file.write(remote_data)
  my_local_file.close

end

ARGV.each do |url|
  obtainImage(url)
end

